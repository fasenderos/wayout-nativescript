export const LANG_IT_NAME = "it";

export const LANG_IT_TRANS = {
  AROUND_YOU: "Intorno a te",
  CANCEL: "Annulla",
  DATE_AND_HOUR: "Data e ora",
  ENTER_THE_ADDRESS: "Inserisci l'indirizzo",
  SEARCH: "Cerca"
};
