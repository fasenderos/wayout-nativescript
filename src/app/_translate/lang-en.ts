export const LANG_EN_NAME = "en";

export const LANG_EN_TRANS = {
  AROUND_YOU: "Aroung You",
  CANCEL: "Cancel",
  DATE_AND_HOUR: "Date and hour",
  ENTER_THE_ADDRESS: "Enter the address",
  SEARCH: "Search"
};
