import { Component } from "@angular/core";
import { EventData, Observable } from "tns-core-modules/data/observable";
import { GooglePlacesAutocomplete } from "nativescript-google-places-autocomplete";

import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { TextField } from "tns-core-modules/ui/text-field";
import { ListView } from "tns-core-modules/ui/list-view";

import { Page } from "tns-core-modules/ui/page";
// import * as observable from "tns-core-modules/data/observable";
// import * as pages from "tns-core-modules/ui/page";
import * as dialogs from "tns-core-modules/ui/dialogs";

const API_KEY = "AIzaSyAOYKrNk8B72AcOnF9SD3WjcemZHmuUcRY";
const googlePlacesAutocomplete = new GooglePlacesAutocomplete(API_KEY);

@Component({
    selector: "AddressAutocomplete",
    moduleId: module.id,
    templateUrl: "./address-autocomplete.component.html",
    styleUrls: ["./address.autocomplete.component.css"]
})
export class AddressAutocompleteComponent extends Observable {
    address = "";
    searchInput = new Subject<string>();
    items;
    private googlePlacesAutocomplete: GooglePlacesAutocomplete;
    private page: Page;
    private events;

    constructor() {
        super();
        this.searchInput.pipe(
            debounceTime(700),
            distinctUntilChanged()
        ).subscribe(
            (params: any) => {
                const list = <ListView>this.page.getViewById("places_list");
                googlePlacesAutocomplete.search(params)
                    .then((places: any) => {
                        this.items = [];
                        this.items = places;
                        this.set("items", this.items);
                        list.items = this.items;
                        list.refresh();
                    }, ((error) => {
                        console.log(error);
                    }));
            },
            (error) => {
                console.log(error);
            }
        );
    }

    getPlace(place) {
        googlePlacesAutocomplete.getPlaceById(place.placeId).then((response) => {
            dialogs.alert("Frmatted address :" + response.formattedAddress + "\n latitude: " + response.latitude + "\n longitude: " + response.longitude)
                .then((result) => {
                    console.log(result);
                });
        }, (error) => {
            console.log(error);
        });
    }

    searchFieldChanged(args: EventData) {
        const tmptextfield = <TextField>args.object;
        this.searchInput
            .next(tmptextfield.text);
    }

    listViewItemTap(args) {
        this.getPlace(this.items[args.index]);
    }

    bindEvents(events) {
        events.forEach((event) => {
            const view = this.page.getViewById(event.viewID);
            if (view) {
                view.on(event.eventName, event.eventHandler, this);
            } else {
                console.log("Error while attempting to bind view with id "
                    + event.viewID);
            }
        });
        return events;
    }

    pageLoaded(args: EventData): void {
        this.page = (args.object as Page);
        this.page.bindingContext = this;
        this.events = this.bindEvents(
            [{
                eventName: "textChange",
                viewID: "searchField",
                eventHandler: this.searchFieldChanged
            }
            ]);

    }
}

const addressAutocompleteComponent = new AddressAutocompleteComponent();
export const pageLoaded = (args) => addressAutocompleteComponent.pageLoaded(args);
export const listViewItemTap = (args) => addressAutocompleteComponent.listViewItemTap(args);
