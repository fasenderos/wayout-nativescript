import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { ModalDialogService } from "nativescript-angular/modal-dialog";

import { AppRoutingModule, COMPONENTS } from "./app-routing.module";
import { AddressAutocompleteComponent } from "./address-autocomplete-component/address-autocomplete.component";
import { AddressPickerComponent } from "./address-picker/address-picker.component";
import { AppComponent } from "./app.component";
import { CoreModule } from "./core/core.module";
import { TranslatePipe, TranslateService, TRANSLATION_PROVIDERS } from "./_translate";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        CoreModule
    ],
    declarations: [
        AppComponent,
        AddressAutocompleteComponent,
        AddressPickerComponent,
        TranslatePipe,
        ...COMPONENTS
    ],
    providers: [
        ModalDialogService,
        TranslateService,
        TRANSLATION_PROVIDERS
    ],
    entryComponents: [
        AddressPickerComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
