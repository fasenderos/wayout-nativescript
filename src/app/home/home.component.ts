import { Component, ElementRef, OnInit, ViewChild, ViewContainerRef } from "@angular/core";
import { DataService, IDataItem } from "../core/data.service";
import { ModalDatetimepicker } from "nativescript-modal-datetimepicker";
import { RouterExtensions } from "nativescript-angular/router";
import { TranslateService } from "../_translate/translate.service";
import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { AddressPickerComponent } from "../address-picker/address-picker.component";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
    @ViewChild("datePicker") datePickerRef: ElementRef;
    @ViewChild("addressPicker") addressPickerReg: ElementRef;
    items: Array<IDataItem>;
    datePicker = new ModalDatetimepicker();
    dateTimeSelected: Date;
    addressSelected;

    constructor(private _translate: TranslateService,
                private itemService: DataService,
                private router: RouterExtensions,
                private modal: ModalDialogService,
                private vcRef: ViewContainerRef) { }

    ngOnInit(): void {
        this.items = this.itemService.getItems();
    }

    openAddressPicker() {
        const options = {
            context: {},
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        this.modal
            .showModal(AddressPickerComponent, options)
            .then((response) => {
                console.log(response);
            });
    }

    onAddressFocus() {
        console.log("ADDRESS FOCUS");
    }

    openDateTimePicker(event) {
        console.log("DATE TIME OPEN", event);
        this.datePicker
            .pickDate({
                theme: "light",
                minDate: this.dateTimeSelected ? this.dateTimeSelected : new Date()
            })
            .then((date) => {
                // Note the month is 1-12 (unlike js which is 0-11)
                console.log(
                    "Date is: " + date.day + "-" + date.month + "-" + date.year
                );
                this.datePicker.pickTime()
                    .then((time) => {
                        console.log("Time is: " + time.hour + ":" + time.minute);
                        this.dateTimeSelected = new Date(date.year, date.month - 1, date.day, time.hour, time.minute);
                        console.log("DATE SELECTED", this.dateTimeSelected);
                        this.datePickerRef.nativeElement.blur();
                    })
                    .catch((error) => {
                        console.log("Error: " + error);
                    });

            })
            .catch((error) => {
                console.log("Error: " + error);
            });
    }
    closeDateTimePicker(event) {
        console.log("DATE TIME CLOSE", event);
    }
}
