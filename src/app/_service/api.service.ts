
import { interval as observableInterval, Observable } from "rxjs";
import { switchMap } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class ApiService {

  // private apiUrl = "https://api.passbot.it/api/"; /** FOR PRODUCTION */
  // private apiUrlShort = "https://api.passbot.it/";

  private apiUrl = "http://127.0.0.1:3000/api/"; /** LOCALHOST */
  private apiUrlShort = "http://127.0.0.1:3000/";

  constructor(private httpClient: HttpClient) { }

  getApi(param: string): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", Authorization: JSON.parse(sessionStorage.getItem("accountAccess")).id });
    return this.httpClient.get<any>(this.apiUrl + param, { headers });
  }
  getApiPolling(time, param: string): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", Authorization: JSON.parse(sessionStorage.getItem("accountAccess")).id });
    return observableInterval(time).pipe(switchMap(() => this.httpClient.get<any>(this.apiUrl + param, { headers })));
  }
  postApi(param: string, content: any): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", Authorization: JSON.parse(sessionStorage.getItem("accountAccess")).id });
    return this.httpClient.post<any>(this.apiUrl + param, content, { headers });
  }
  login(param: string, content: any): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    return this.httpClient.post<any>(this.apiUrl + param, content, { headers });
  }
  registration(param: string, content: any): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    return this.httpClient.post<any>(this.apiUrlShort + param, content, { headers });
  }
  postApiShort(param: string, content: any): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", Authorization: JSON.parse(sessionStorage.getItem("accountAccess")).id });
    return this.httpClient.post<any>(this.apiUrlShort + param, content, { headers });
  }
  getApiShort(param: string) {
    const headers = new HttpHeaders({ "Content-Type": "application/json", Authorization: JSON.parse(sessionStorage.getItem("accountAccess")).id });
    return this.httpClient.get<any>(this.apiUrlShort + param, { headers });
  }
  putApi(param: string, content: any): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", Authorization: JSON.parse(sessionStorage.getItem("accountAccess")).id });
    return this.httpClient.put<any>(this.apiUrl + param, content, { headers });
  }
  deleteApi(param: string): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", Authorization: JSON.parse(sessionStorage.getItem("accountAccess")).id });
    return this.httpClient.delete<any>(this.apiUrl + param, { headers });
  }
  printApi(param: string, content: any): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", Authorization: JSON.parse(sessionStorage.getItem("accountAccess")).id });
    return this.httpClient.post<any>(this.apiUrlShort + param, content, { headers });
  }
  patchApi(param: string, content: any): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", Authorization: JSON.parse(sessionStorage.getItem("accountAccess")).id });
    return this.httpClient.patch<any>(this.apiUrl + param, content, { headers });
  }
  sclobySyncApi(param: string, content: any): Observable<any> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", Authorization: JSON.parse(sessionStorage.getItem("accountAccess")).id });
    return this.httpClient.post<any>(this.apiUrlShort + param, content, { headers });
  }
  logout() {
    window.sessionStorage.clear();
  }
}
