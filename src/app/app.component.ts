import { Component, OnInit } from "@angular/core";
import { device, isAndroid, isIOS, screen } from "tns-core-modules/platform";
import { Device, Screen } from "./_models/index";
import { TranslateService } from "./_translate/translate.service";

@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "app.component.html",
    styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
    deviceInformation;
    screenInformation;
    supportedLanguages: Array<string> = ["en", "it"];

    constructor(private _translate: TranslateService) {
        // Use the component constructor to inject providers.
        this.deviceInformation = new Device(
            device.model,
            device.model,
            device.os,
            device.osVersion,
            device.sdkVersion,
            device.language,
            device.manufacturer,
            device.uuid);
        this.screenInformation = new Screen(
            screen.mainScreen.heightDIPs,
            screen.mainScreen.heightPixels,
            screen.mainScreen.scale,
            screen.mainScreen.widthDIPs,
            screen.mainScreen.widthPixels);
    }

    ngOnInit(): void {
        // Init your component properties here.
        console.log("device information", this.deviceInformation);

        // subscribe to language changes
        this.subscribeToLangChanged();

        // set language
        this._translate.setDefaultLang("en"); // set English as default
        this._translate.enableFallback(true); // enable fallback

        // set current langage
        const langIndex = this.supportedLanguages.indexOf(this.deviceInformation.language);
        if (langIndex > -1) {
            this.selectLang(this.deviceInformation.language);
        } else {
            this.selectLang("en");
        }

        console.log("TRADUZIONE", this._translate.instant("HOME"));

    }

    getIconSource(icon: string): string {
        const iconPrefix = isAndroid ? "res://" : "res://tabIcons/";

        return iconPrefix + icon;
    }

    selectLang(lang: string) {
        // set current lang;
        this._translate.use(lang);
    }

    subscribeToLangChanged() {
        // refresh text
        // please unsubribe during destroy
        console.log("language change in: ", this.deviceInformation.language);
    }
}
