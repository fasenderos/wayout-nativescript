import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/directives/dialogs";
import { AddressAutocompleteComponent } from "../address-autocomplete-component/address-autocomplete.component";

@Component({
    selector: "AddressPicker",
    moduleId: module.id,
    templateUrl: "./address-picker.component.html",
    styleUrls: ["./address.picker.component.css"]
})
export class AddressPickerComponent {

    constructor(private modalParams: ModalDialogParams) { }

    close(response) {
        console.log(response);
        this.modalParams.closeCallback(response);
    }
}
